# Polisseur

Machine de polissage de pièce (notamment imprimées 3D)

<img src="https://framagit.org/arofarn/polisseur/raw/master/doc/pictures/Polisseur.jpg" width=640>

Très largement inspirée du projet "Affordable Rock Tumbler" (https://www.thingiverse.com/thing:935252) de J_T_3_D (https://www.thingiverse.com/J_T_3_D/about).

Presque entièrement redessinée pour que le cadre soit découpé à la fraisseuse à commande numérique et pour utiliser les pièces que j'avais en stock:
* structure en PEHD de 15mm d'épaisseur (à la place de l'impression 3D)
* moteur pas-à-pas NEMA 17 + électronique de contrôle (à la place du moteur à courant continu)
* de la tige fileté 8mm (axes de rotation) et 10mm (structure) à la place de 8mm partout (pas assez en stock).

## Améliorations par rapport au projet d'origine:
* l'électronique permet d'ajuster la vitesse et intégre un compte-à-rebour
* base en PEHD plus lourde, largement surdimensionnée, très stable
* le moteur peut être monté à droite ou à gauche
* la structure peut-être facilement usinée à la fraiseuse à commande numérique dans du plastique (PEHD ou autre), du bois ou du métal (aluminium). L'impression 3D de la structure reste possible aussi, mais consommera plus de plastique que la version originale

## Liste de matériel:
### Structure
* plaque de plastique (PEHD par exemple) usinable, bois ou aluminium de 15mm d'épaisseur (possible d'ajuster à d'autres épaisseurs dans le fichier FreeCAD) pour y usiner les quatre pièces suivantes :
  * 2 ''spacer''
  * 1 ''frame'' droite
  * 1 ''frame'' gauche (symétrique à la droite)
* 2 tiges filetées M10, longueur 255mm
* 8 écrous M10 (idéal : 8, minimum 4)
* 8 rondelles M10 (pas indispensable mais idéal = nombre d'écrous)

### Parties mobiles
* Moteur pas-à-pas NEMA17
* 5 vis M3 longueur 12mm (fixation du moteur sur la structure + serrage de l'engrenage sur l'axe du moteur)
* 1 écrou carré M3 ou à défaut un écrou classique (héxagonal)
* 1 tige filetée M8, longueur 240mm
* 1 tige filetée M8, longueur 260mm
* 18 écrous M8
* 4 rondelles M8 (optionnelles)
* 4 roulements à billes 608 (diamètre externe 22mm, interne 8mm, épaisseur 7mm)
* 4 joints torique n°16 (19,8X27X3,6) pour les poulies
* 4 poulies (impression 3D)
* 1 grand engrenage (impression 3D)
* 1 petit engrenage (impression 3D)

### Electronique
* 1 afficheur avec microcontrôleur Sparkfun Serial 7-Segments Display (https://www.sparkfun.com/products/11441)
* 1 pilote de moteur pas-à-pas Stepstick (https://reprap.org/wiki/StepStick)
* 1 condensateur 100µF 16V (peut varier en fonction du pilote de moteur pas-à-pas)
* 1 encodeur rotatif
* 1 plaque perforée pour circuit électronique Adafruit Half-size perma-proto breadboard (https://www.adafruit.com/product/1609)
* 1 connecteur 4 pins à souder adapté au moteur pas-à-pas (facultatif si on soude directement le cable du moteur sur le circuit)
* 2 header mâles 8 broches pour brancher le driver (optionnels à la place on peut souder le driver directement sur le circuit, mais il sera plus difficile à changer en cas de panne)
* quelques fils de couleurs variées pour connecter le tout
* un connecteur d'alimentation type bornier-connecteur montable en façade, 3 broches (pour 5V, 12V et masse)
* alimentation 5V (mini 500mA) et 12V (mini 2A)
* 3 vis M3 longeur 5 à 8mm:
  * 1 vis pour fixer le circuit au boitier
  * 2 vis pour le connecteur d'alimentation (si vous utiliser le même que moi)
* 1 boîtier:
  * 1 boitier ''electronic_enclosure.stl'' (impression 3D)
  * 1 couvercle ''electronic_cover.stl'' (impression 3D)
  * 4 vis M3 8 à 12mm
  * 4 rondelles M3

### Bidon
* manchon de PVC diamètre 100 ou 125mm
* 2 couvercles vissables (ou un vissable et un borgne) du même diamètre
* colle PVC
* insert (impression 3D, si possible en flex, échelle à adapter au dimension du bidon) (optionnel)
* 2 ronds de mousse EVA diamètre interne du manchon PVC (à coller sur les couvercles vissable pour atténuer le bruit) (optionnel)
* matériaux de polissage : nombreuses petites vis à bois, sables abrasifs, ...

### Outillage
  * fraisseuse numérique (remplacable par une imprimante 3D)
    * fraises pour le PEHD de 15mm :
      * 1 fraise 1 dent spirale diamètre 6mm profondeur de coupe 20 mm (16mm minimum)
      * 1 fraise 1 dent spirale diamètre 3mm profondeur de coupe 10 mm (pour les 4 oblongs de la visserie du moteur)
  * imprimante 3D
  * clés plates de 13 et de 17
  * clé à molette
  * jeux de clés Allen ou tournevis (suivant le type de vis M3)
  * fer à souder + accessoires

## Remarques

### Structure

Les fichiers DXF du répertoire ''3D/cnc_router'' sont prêts pour un import dans un logiciel de fraisage comme GALAAD. Les tolérances sont prises en compte, ainsi que le fraisage de angles.
Prévoir que les poches des roulements et du moteur font 7mm. Le reste est de la découpe.
Attention aussi aux quatre oblongs pour la fixation du moteur, à réaliser avec une fraise de 3mm maximum (8mm de profondeur de découpe => idéal fraise avec un hauteur de coupe de 10mm au moins).

On peut réaliser la structure dans d'autres matériaux (autre matière plastiques, bois, aluminium) ou d'autres épaisseurs. En cas de changement de matière ou d'épaisseur, il faut prendre en compte quelques points:
* le perçage de l'emboitement des partie avant/arrière dans les flancs doit tenir compte de l'épaisseur réel du matériaux
* les poches pour les roulements doivent faire 7mm de profondeurs (épaisseur des roulements)
* le moteur doit avoir une poche suffisante pour que l'axe dépasse de 15 à 20 mm pour une bonne prise sur l'engrenage
* différent matériaux peuvent avoir différentes tolérances, faire un test! en particulier pour l'emboitement du cadre et les poches des roulements

### Electronique

A propos de la partie électronique, la carte Sparkfun Serial 7-Segments Display intégre le microcontroleur Atmel ATMEGA 328, identique à un Arduino UNO R3. Cependant, il est nécessaire d'utiliser une carte intermédiaire pour la programmer. Personnellement, j'utilise une carte FTDI Friend d'Adafruit, mais il est possible de faire la même chose avec une carte Arduino UNO R3 dont le microcontroleur a été temporairement enlevé.

On peut donc tout à fait remplacer par un Arduino UNO R3 ou équivalent avec un afficheur 7 segments 4 digits. Dans ce cas, le boitier n'est plus du tout adapté et il ne sert à rien de l'imprimer.

Comme le pilote du moteur avec son petit radiateur et sur son connecteur très haut, je l'ai soudé au dos du circuit (par rapport à la carte avec l'afficheur et l'encodeur rotatif). Attention! Le cablage du pilote tel que soudé sur le circuit final est donc inversé par rapport au montage sur breadboard. 

En choisant l'emplacement de l'encodeur, avant de souder, il faut faire attention à laisser la place nécessaire pour la vis de fixation.

On peut souder "en surface" des broches classiques coudées à 90° sur les connecteurs CTR/RX/TX/VCC/GND sur le côté de l'afficheur. Ca aidera à la connection pour flasher le firmware.

### Bidon

Les éléments dans le BOM sont donnés à titre indicatif : ça peut être n'importe quoi de bien rond qui résiste correctement à l'abrasion.

Idem pour le matériau abrasif : faites vos tests et adaptez à au objet à polir.
